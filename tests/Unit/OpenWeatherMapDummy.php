<?php

namespace Tests\Unit;

use App\Externals\OpenWeatherMap;
use App\Models\City;
use Carbon\Carbon;

class OpenWeatherMapDummy extends OpenWeatherMap
{
    public static $cityPosition;
    public static $forecastWeather;

    public function getCityPosition($cityName): array
    {
        return static::$cityPosition ?? [];
    }

    public function forecastWeather(City $city): array
    {
        return static::$forecastWeather ?? [];
    }

    public static function reset()
    {
        static::$cityPosition = [];
        static::$forecastWeather = [];
    }
}
