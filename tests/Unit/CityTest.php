<?php

namespace Tests\Unit;

use App\Contracts\GeoResolver;
use App\DTO\Position;
use App\Models\City;
use Tests\TestCase;

class CityTest extends TestCase
{
    public function test_when_city_doesnt_have_position_then_should_fill_geo()
    {
        $this->app->bind(GeoResolver::class, fn() => new class implements GeoResolver {
            public function getCityPosition(string $cityName): Position
            {
                return new Position(1, 2);
            }
        });

        $city = City::create(['name' => 'test']);

        $this->assertEquals(1, $city->lat);
        $this->assertEquals(2, $city->lon);
    }
}
