<?php

use App\Models\City;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('weather_forecasts', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(City::class)->constrained();
            $table->date('date');
            $table->json('response');
            $table->timestamps();
            $table->index(['date', 'city_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('weather_forecast');
    }
};
