<?php

namespace Database\Seeders;

use App\Externals\OpenWeatherMap;
use App\Models\City;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    private $cities = [
        ['name' => 'new york', 'lat' => '40.7127281', 'lon' => '-74.0060152'],
        ['name' => 'london', 'lat' => '51.5073219', 'lon' => '-0.1276474'],
        ['name' => 'paris', 'lat' => '48.8588897', 'lon' => '2.3200410217201'],
        ['name' => 'berlin', 'lat' => '52.5170365', 'lon' => '13.3888599'],
        ['name' => 'tokyo', 'lat' => '35.6828387', 'lon' => '139.7594549'],
    ];

    public function run()
    {
        collect($this->cities)->each(fn($city) => City::create($city));
    }
}
