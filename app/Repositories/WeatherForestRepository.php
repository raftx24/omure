<?php

namespace App\Repositories;

use App\Contracts\WeatherForecastResolver;
use App\Models\City;
use App\Models\WeatherForecast;

class WeatherForestRepository implements WeatherForecastResolver
{
    public function forecastWeather(City $city, $date): array
    {
        return WeatherForecast::whereCityId($city->id)
            ->where('date', $date)
            ->first()
            ?->response ?? [];
    }
}
