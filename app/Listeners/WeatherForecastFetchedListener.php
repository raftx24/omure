<?php

namespace App\Listeners;

use App\Events\WeatherForecastFetched;
use App\Models\WeatherForecast;

class WeatherForecastFetchedListener
{
    public function __construct()
    {
        //
    }

    public function handle(WeatherForecastFetched $event)
    {
        collect($event->result['daily'])
            ->each(function ($response) use ($event) {
                (new WeatherForecast([
                    'date' => $response['dt'],
                    'response' => $response,
                ]))
                    ->city()->associate($event->city)
                    ->save();
            });
    }
}
