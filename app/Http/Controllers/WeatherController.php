<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Services\WeatherForecast;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WeatherController extends Controller
{
    public function index(Request $request, WeatherForecast $weatherForecast)
    {
        return $weatherForecast->forecastWeather(
            City::whereName($request->get('city'))->firstOrFail(),
            Carbon::parse($request->get('date'))
        );
    }
}
