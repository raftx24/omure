<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WeatherIndexRequest extends FormRequest
{
    public function authorize()
    {
    }

    public function rules()
    {
        return [
            'date' => 'required|date',
            'city' => 'required|exists:cities,name'
        ];
    }
}
