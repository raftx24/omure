<?php

namespace App\Events;

use App\Models\City;

class WeatherForecastFetched
{
    public function __construct(
        public City  $city,
        public array $result,
    )
    {
    }
}
