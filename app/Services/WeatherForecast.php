<?php

namespace App\Services;

use App\Contracts\WeatherForecastResolver;
use App\Models\City;
use App\Repositories\WeatherForestRepository;

class WeatherForecast implements WeatherForecastResolver
{
    private array $strategies = [
        WeatherForestRepository::class,
        WeatherForecastResolver::class,
    ];

    public function updateCurrentWeathers()
    {
        City::all()
            ->each(fn (City $city) => resolve(WeatherForecastResolver::class)->forecastWeather($city));
    }

    public function forecastWeather(City $city, $date): array
    {
        foreach ($this->strategies as $strategy) {
            $result = resolve($strategy)->forecastWeather($city, $date);

            if (count($result)) {

                print_r($result);
                die();
                return $result;
            }
        }

        throw new \Exception();
    }
}
