<?php

namespace App\Services;

use App\Contracts\GeoResolver;
use App\Contracts\WeatherForecastResolver;
use App\DTO\Position;
use App\Events\WeatherForecastFetched;
use App\Exceptions\CityIsNotValidException;
use App\Models\City;
use App\Models\WeatherForecast;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class OpenWeatherMap implements GeoResolver, WeatherForecastResolver
{
    private $api;

    public function __construct(\App\Externals\OpenWeatherMap $api)
    {
        $this->api = $api;
    }

    public function getCityPosition($cityName): Position
    {
        return collect($this->api->getCityPosition($cityName))
            ->whenNotEmpty(
                fn ($result) => new Position($result[0]['lat'], $result[0]['lon']),
                fn () => throw new CityIsNotValidException(),
            );
    }

    public function currentWeather(City $city): array
    {
        $result = $this->sendRequest(
            'data/2.5/onecall',
            [
                'lat' => $city->lat,
                'lon' => $city->lon,
                'exclude' => 'hourly,daily'
            ]
        );

        event(new WeatherForecastFetched($city, $result));

        return $result;
    }

    public function forecastWeather(City $city, Carbon $date): array
    {
        $result = $this->api->forecastWeather($city);

//        print_r($result['daily']);
        event(new WeatherForecastFetched($city, $result));

        return collect($result['daily'])
            ->first(fn ($daily) => Carbon::createFromTimestamp($daily['dt'])->isSameDay($date)) ?? [];
    }
}
