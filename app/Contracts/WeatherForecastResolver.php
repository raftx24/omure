<?php

namespace App\Contracts;

use App\Models\City;
use Carbon\Carbon;

interface WeatherForecastResolver
{
    public function forecastWeather(City $city, Carbon $date): array;
}
