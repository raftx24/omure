<?php

namespace App\Contracts;

use App\DTO\Position;

interface GeoResolver
{
    public function getCityPosition(string $cityName): Position;
}
