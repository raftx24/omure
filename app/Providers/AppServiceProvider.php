<?php

namespace App\Providers;

use App\Contracts\GeoResolver;
use App\Contracts\WeatherForecastResolver;
use App\Events\WeatherForecastFetched;
use App\Listeners\WeatherForecastFetchedListener;
use App\Models\City;
use App\Observers\CityObserver;
use App\Services\OpenWeatherMap;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GeoResolver::class, OpenWeatherMap::class);
        $this->app->bind(WeatherForecastResolver::class, OpenWeatherMap::class);

        Event::listen([WeatherForecastFetched::class], WeatherForecastFetchedListener::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        City::observe(CityObserver::class);
    }
}
