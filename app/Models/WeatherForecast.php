<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeatherForecast extends Model
{
    use HasFactory;

    protected $fillable = ['response', 'date'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
