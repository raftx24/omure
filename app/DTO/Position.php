<?php

namespace App\DTO;

class Position
{
    public function __construct(
        public string $lat,
        public string $lon,
    )
    {
    }
}
