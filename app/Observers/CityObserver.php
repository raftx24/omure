<?php

namespace App\Observers;

use App\Contracts\GeoResolver;
use App\Models\City;

class CityObserver
{
    public function creating(City $city)
    {
        if (!$city->lat || !$city->lon) {
            $pos = resolve(GeoResolver::class)->getCityPosition($city->name);

            $city->fill([
                'lat' => $pos->lat,
                'lon' => $pos->lon,
            ]);
        }
    }
}
