<?php

namespace App\Externals;

use App\Models\City;
use Illuminate\Support\Facades\Http;

class OpenWeatherMap
{
    public function getCityPosition($cityName): array
    {
        return $this->sendRequest('geo/1.0/direct', [
            'q' => $cityName,
            'limit' => 1,
        ]);
    }

    public function forecastWeather(City $city): array
    {
        return $this->sendRequest(
            'data/2.5/onecall',
            [
                'lat' => $city->lat,
                'lon' => $city->lon,
                'exclude' => 'hourly,minutely'
            ]
        );
    }

    private function sendRequest($route, $params)
    {
        return Http::get(
            sprintf('https://api.openweathermap.org/%s', $route),
            $params + ['appid' => config('openweathermap.apiKey')]
        )->json();
    }
}
